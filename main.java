import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class WordProcessor {

	protected Shell mainBox;
	private Text mainTextBox;
	private Text outputFileName;
	private String inFileName = "";
	private String outFileName = "";
	private int lineHolderCounter = 0;
	private int wordsProcessed, lines , spaceCount, blankLines = 0; 
	private int aveWordLine, aveLineLength = 0;
	private Label errorLabel;
	private Button leftRadioButton;
	private Boolean leftSideSelect  = true; 
	private Boolean boxSideSelect = false;
	private Boolean singleSpaceSelect = true;
	private Text characterLimitTextbox;
	
	
	/**
	 * main application to launch window
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			WordProcessor window = new WordProcessor();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Opens the window
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mainBox.open();
		mainBox.layout();
		while (!mainBox.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mainBox = new Shell();
		mainBox.setSize(691, 425);
		mainBox.setText("CSE 360 Word Processor");
		mainBox.setLayout(new FormLayout());
		
		mainTextBox = new Text(mainBox, SWT.BORDER | SWT.READ_ONLY | SWT.WRAP);
		FormData fd_mainTextBox = new FormData();
		fd_mainTextBox.right = new FormAttachment(100, -10);
		fd_mainTextBox.left = new FormAttachment(0, 369);
		mainTextBox.setLayoutData(fd_mainTextBox);
		
		Label fileSelectLabel = new Label(mainBox, SWT.NONE);
		fd_mainTextBox.top = new FormAttachment(fileSelectLabel, 0, SWT.TOP);
		FormData fd_fileSelectLabel = new FormData();
		fd_fileSelectLabel.left = new FormAttachment(0, 16);
		fd_fileSelectLabel.top = new FormAttachment(0, 10);
		fileSelectLabel.setLayoutData(fd_fileSelectLabel);
		fileSelectLabel.setText("Please select a file to process");
		
		Button fileSelectButton = new Button(mainBox, SWT.NONE);	
		
		FormData fd_fileSelectButton = new FormData();
		fd_fileSelectButton.top = new FormAttachment(fileSelectLabel, 6);
		fd_fileSelectButton.left = new FormAttachment(fileSelectLabel, 0, SWT.LEFT);
		fileSelectButton.setLayoutData(fd_fileSelectButton);
		fileSelectButton.setText("File Select");
		
		Label ouputFileLabel = new Label(mainBox, SWT.NONE);
		FormData fd_ouputFileLabel = new FormData();
		fd_ouputFileLabel.top = new FormAttachment(fileSelectButton, 7);
		fd_ouputFileLabel.left = new FormAttachment(fileSelectLabel, 0, SWT.LEFT);
		ouputFileLabel.setLayoutData(fd_ouputFileLabel);
		ouputFileLabel.setText("Please type the name of what you\r\nwant to name the output file (.txt)");
		
		outputFileName = new Text(mainBox, SWT.BORDER);
		FormData fd_outputFileName = new FormData();
		fd_outputFileName.top = new FormAttachment(ouputFileLabel, 6);
		fd_outputFileName.right = new FormAttachment(fileSelectLabel, 23, SWT.RIGHT);
		fd_outputFileName.left = new FormAttachment(fileSelectLabel, 0, SWT.LEFT);
		outputFileName.setLayoutData(fd_outputFileName);
		
		Label processingFileLabel = new Label(mainBox, SWT.NONE);
		FormData fd_processingFileLabel = new FormData();
		fd_processingFileLabel.left = new FormAttachment(fileSelectLabel, 0, SWT.LEFT);
		processingFileLabel.setLayoutData(fd_processingFileLabel);
		processingFileLabel.setText("Press button to process file");
		
		Button processFileButton = new Button(mainBox, SWT.NONE);
		fd_mainTextBox.bottom = new FormAttachment(processFileButton, 0, SWT.BOTTOM);
		
		fd_processingFileLabel.bottom = new FormAttachment(100, -36);
		FormData fd_processFileButton = new FormData();
		fd_processFileButton.top = new FormAttachment(processingFileLabel, 6);
		fd_processFileButton.left = new FormAttachment(fileSelectLabel, 0, SWT.LEFT);
		fd_processFileButton.bottom = new FormAttachment(100, -10);
		processFileButton.setLayoutData(fd_processFileButton);
		processFileButton.setText("Process File");
		
		Label InputFileLabel = new Label(mainBox, SWT.NONE);
		FormData fd_InputFileLabel = new FormData();
		fd_InputFileLabel.right = new FormAttachment(fileSelectButton, 0, SWT.RIGHT);
		InputFileLabel.setLayoutData(fd_InputFileLabel);
		InputFileLabel.setText("Input File:  ");
		
		Label inputFileLabelName = new Label(mainBox, SWT.NONE);
		FormData fd_inputFileLabelName = new FormData();
		fd_inputFileLabelName.top = new FormAttachment(InputFileLabel, 0, SWT.TOP);
		fd_inputFileLabelName.right = new FormAttachment(ouputFileLabel, 16, SWT.RIGHT);
		fd_inputFileLabelName.left = new FormAttachment(InputFileLabel, 22);
		inputFileLabelName.setLayoutData(fd_inputFileLabelName);
		inputFileLabelName.setText(inFileName);
		
		Button OutputNameButton = new Button(mainBox, SWT.NONE);
		
		fd_InputFileLabel.top = new FormAttachment(OutputNameButton, 6);
		FormData fd_OutputNameButton = new FormData();
		fd_OutputNameButton.top = new FormAttachment(outputFileName, 6);
		fd_OutputNameButton.left = new FormAttachment(fileSelectLabel, 0, SWT.LEFT);
		OutputNameButton.setLayoutData(fd_OutputNameButton);
		OutputNameButton.setText("Set Ouput");
		
		Label outputFileLabel = new Label(mainBox, SWT.NONE);
		FormData fd_outputFileLabel = new FormData();
		fd_outputFileLabel.top = new FormAttachment(InputFileLabel, 6);
		fd_outputFileLabel.left = new FormAttachment(fileSelectLabel, 0, SWT.LEFT);
		outputFileLabel.setLayoutData(fd_outputFileLabel);
		outputFileLabel.setText("Output File: ");
		
		Label outputFileLabelName = new Label(mainBox, SWT.NONE);
		FormData fd_outputFileLabelName = new FormData();
		fd_outputFileLabelName.top = new FormAttachment(outputFileLabel, 0, SWT.TOP);
		fd_outputFileLabelName.left = new FormAttachment(outputFileLabel, 17);
		fd_outputFileLabelName.right = new FormAttachment(inputFileLabelName, 0, SWT.RIGHT);
		outputFileLabelName.setLayoutData(fd_outputFileLabelName);
		outputFileLabelName.setText(outFileName);
		
		leftRadioButton = new Button(mainBox, SWT.RADIO);
		leftRadioButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				leftSideSelect = true;
				boxSideSelect = false;
			}
		});
		FormData fd_leftRadioButton = new FormData();
		fd_leftRadioButton.top = new FormAttachment(outputFileLabel, 3);
		fd_leftRadioButton.left = new FormAttachment(fileSelectLabel, 0, SWT.LEFT);
		leftRadioButton.setLayoutData(fd_leftRadioButton);
		leftRadioButton.setText("Left Justification");
		leftRadioButton.setSelection(true);
		
		Button rightRadioButton = new Button(mainBox, SWT.RADIO);
		rightRadioButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				leftSideSelect = false;
				boxSideSelect = false;
			}
		});
		FormData fd_rightRadioButton = new FormData();
		fd_rightRadioButton.top = new FormAttachment(leftRadioButton, 0, SWT.TOP);
		fd_rightRadioButton.left = new FormAttachment(leftRadioButton);
		rightRadioButton.setLayoutData(fd_rightRadioButton);
		rightRadioButton.setText("Right Justification");
		
		errorLabel = new Label(mainBox, SWT.NONE);
		errorLabel.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		errorLabel.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		FormData fd_errorLabel = new FormData();
		fd_errorLabel.bottom = new FormAttachment(fileSelectButton, 0, SWT.BOTTOM);
		fd_errorLabel.left = new FormAttachment(fileSelectLabel, 18);
		fd_errorLabel.right = new FormAttachment(mainTextBox, -6);
		fd_errorLabel.top = new FormAttachment(0, 8);
		errorLabel.setLayoutData(fd_errorLabel);
		
		Button clearButton = new Button(mainBox, SWT.NONE);
		clearButton.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				mainTextBox.setText("");
				outputFileName.setText("");
				inFileName = "";
				outFileName = "";
				lineHolderCounter = 0;
				wordsProcessed = 0;
				lines = 0;
				blankLines = 0; 
				aveWordLine = 0;
				aveLineLength = 0;
				outputFileLabelName.setText("");
				inputFileLabelName.setText("");
				errorLabel.setText("");
			}
		});
		clearButton.addSelectionListener(new SelectionAdapter() {
			
		});
		FormData fd_clearButton = new FormData();
		fd_clearButton.bottom = new FormAttachment(processingFileLabel, -6);
		fd_clearButton.left = new FormAttachment(fileSelectLabel, 0, SWT.LEFT);
		clearButton.setLayoutData(fd_clearButton);
		clearButton.setText("CLEAR");
		
		Label clearLabel = new Label(mainBox, SWT.NONE);
		FormData fd_clearLabel = new FormData();
		fd_clearLabel.top = new FormAttachment(leftRadioButton, 52);
		fd_clearLabel.right = new FormAttachment(leftRadioButton, 0, SWT.RIGHT);
		fd_clearLabel.left = new FormAttachment(0, 16);
		clearLabel.setLayoutData(fd_clearLabel);
		clearLabel.setText("Click this button \r\nto clear everything");
		
		Button boxRadioButton = new Button(mainBox, SWT.RADIO);
		boxRadioButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				leftSideSelect = true;
				boxSideSelect = true;
			}
		});
		boxRadioButton.addMouseListener(new MouseAdapter() {
			
		});
		FormData fd_boxRadioButton = new FormData();
		fd_boxRadioButton.top = new FormAttachment(leftRadioButton, 0, SWT.TOP);
		fd_boxRadioButton.left = new FormAttachment(rightRadioButton, 6);
		boxRadioButton.setLayoutData(fd_boxRadioButton);
		boxRadioButton.setText("Box Justification");
		
		Label characterLimitLabel = new Label(mainBox, SWT.NONE);
		FormData fd_characterLimitLabel = new FormData();
		fd_characterLimitLabel.top = new FormAttachment(clearLabel, 0, SWT.TOP);
		fd_characterLimitLabel.left = new FormAttachment(clearLabel, 48);
		characterLimitLabel.setLayoutData(fd_characterLimitLabel);
		characterLimitLabel.setText("Enter Character limit:");
		
		characterLimitTextbox = new Text(mainBox, SWT.BORDER);
		FormData fd_characterLimitTextbox = new FormData();
		fd_characterLimitTextbox.right = new FormAttachment(mainTextBox, -6);
		fd_characterLimitTextbox.left = new FormAttachment(mainTextBox, -106, SWT.LEFT);
		fd_characterLimitTextbox.top = new FormAttachment(clearLabel, -3, SWT.TOP);
		characterLimitTextbox.setLayoutData(fd_characterLimitTextbox);
		characterLimitTextbox.setText("80");
		
		Group spaceGroup = new Group(mainBox, SWT.NONE);
		spaceGroup.setText("Single or Double Space?");
		FormData fd_spaceGroup = new FormData();
		fd_spaceGroup.left = new FormAttachment(clearLabel, 21);
		fd_spaceGroup.right = new FormAttachment(mainTextBox, -6);
		fd_spaceGroup.bottom = new FormAttachment(clearButton, 0, SWT.BOTTOM);
		fd_spaceGroup.top = new FormAttachment(characterLimitLabel, 6);
		spaceGroup.setLayoutData(fd_spaceGroup);
		
		Button singleSpaceRadioButton = new Button(spaceGroup, SWT.RADIO);
		singleSpaceRadioButton.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				singleSpaceSelect = true;
			}
		});
		singleSpaceRadioButton.setBounds(10, 18, 90, 16);
		singleSpaceRadioButton.setText("Single Space");
		singleSpaceRadioButton.setSelection(true);
		
		Button doubleSpaceRadioButton = new Button(spaceGroup, SWT.RADIO);
		doubleSpaceRadioButton.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				singleSpaceSelect = false;
			}
		});
		doubleSpaceRadioButton.setBounds(106, 18, 90, 16);
		doubleSpaceRadioButton.setText("Double Space");
		FormData fd_doubleSpaceRadioButton = new FormData();
		fd_doubleSpaceRadioButton.bottom = new FormAttachment(mainTextBox, 0, SWT.BOTTOM);
		
		
		fileSelectButton.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				try {
					JFileChooser fileChooser = new JFileChooser();
					fileChooser.setDialogTitle("Open TXT File");
					fileChooser.showOpenDialog(null);
					inFileName = fileChooser.getSelectedFile().getName();
					inputFileLabelName.setText(inFileName);
					
					String line = "";
					
					
					FileReader inputFile = new FileReader(inFileName);
					BufferedReader bufferedReader = new BufferedReader(inputFile);
					while((line = bufferedReader.readLine()) != null) { }
					bufferedReader.close();
				}
				catch (Exception ex) {
					errorLabel.setText("There was an error in \nreading the file");
				}
				
				
			}
		});

		OutputNameButton.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				outFileName = outputFileName.getText();
				outputFileName.setText("");
				outputFileLabelName.setText(outFileName);
			}
		});
		
		processFileButton.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				if (outFileName != "")
				{
					try {
						fileProcessing();
						
						errorLabel.setText("");
					}
					catch(Exception ex) {
						errorLabel.setText("There was an error in either \nreading or writing to your files.");
					}
				}
				else if (inFileName == "")
				{
					errorLabel.setText("Please select the .txt file\nyou want to use");
				}
				else
				{
					errorLabel.setText("Please write the name of\nyour output txt file");
				}
			}
		});
	}
	
	/* this method gets the input
	 * file and process the input 
	 * variables and sets them
	 * for analyzing
	 */
	public void fileProcessing()
	{
		wordsProcessed = 0;
		lines = 0;
		blankLines = 0;
		aveWordLine = 0;
		aveLineLength = 0; 
		String line, newLine = "";
		String[] words;
		ArrayList<String> lineHolder = new ArrayList();
	
		try
		{
			FileReader inputFile = new FileReader(inFileName);
			BufferedReader bufferedReader = new BufferedReader(inputFile);
			
			while((line = bufferedReader.readLine()) != null)
			{
				newLine = line.replaceAll("\t", " ");
				words = newLine.split(" "); 
				
				int blank = 0;
				
				try 
				{
					for (int i = 0; words[i] != null; i++)
					{
						if (words[i].equals(""))
						{
							blank++;
						}
						else
						{
							wordsProcessed++;
							lineHolder.add(words[i]);
							lineHolderCounter++;
						}
					}
				}
				catch (Exception ex) { }
				
				if (blank > 0 && blank < 2)
					blankLines++;
			}
			
			bufferedReader.close();
		
		}
		catch (Exception ex) { }
		
		outputFile(lineHolder);
		
		mainTextBox.setText("\n\n\nInput File: "+ inFileName
				+ "\nWords processed:\t\t" + wordsProcessed
				+ "\nNumber of lines:\t\t" + lines 
				+ "\nBlank lines removed:\t" + blankLines
				+ "\nAverage words per line:\t" + aveWordLine
				+ "\nAverage line length:\t" + aveLineLength
				+ "\nSpace Count:\t\t" + spaceCount );
	}
	
	/* this method process the output file and 
	 * sets the analyzing variables to the right
	 * numbers
	 */
	public void outputFile(ArrayList<String> lineHolder)
	{	
		int counter = 0;
		int lineLength = 0;
		String word = "";
		String Character = "";
		int maxCharacters = Integer.parseInt(characterLimitTextbox.getText());
		
		try
		{
			PrintWriter writer = new PrintWriter(outFileName);
		
			while(counter < lineHolderCounter)
			{
				if (lineHolder.isEmpty())
					break;
				
				Character += lineHolder.get(counter);
				if ((Character.length()) < maxCharacters)
				{
					Character += " ";
					word = Character;
					spaceCount++;
				}
				else if (lineHolder.get(counter).length() > maxCharacters)
				{	
					int aligmentSize = maxCharacters - word.length();
					String Space = "";
					
					for (int i = 0; i < aligmentSize; i ++)
					{
						Space += " ";
					}
					
					
					if (word.equals("")){}
					else
					{
						if (leftSideSelect == true)
						{
							writer.println(word);
							
							if (singleSpaceSelect == true)
							{
								lines++;
							}
							else
							{
								lines = lines + 2;
								writer.println("\n");
							}
							
						}
						else
						{
							writer.println(Space + word);
							
							if (singleSpaceSelect == true)
							{
								lines++;
							}
							else
							{
								lines = lines + 2;
								writer.println("\n");
							}
						}
						
						lineLength += word.length();
					}					
					
					writer.println(lineHolder.get(counter));
					
					lines++;
					lineLength += lineHolder.get(counter).length();
					
					word = "";
					Character = "";
				}
				else
				{
					int aligmentSize = (maxCharacters - word.length());
					String Space = "";
					
					for (int i = 0; i < aligmentSize; i ++)
					{
						Space += " ";
					}
					
					lineLength += word.length();
					
					if (leftSideSelect == true)
					{
						
						int i = 0;
						
						String parseWord = lineHolder.get(counter);
						String[] parseWordArr = parseWord.split("");
						
						if (boxSideSelect == true)
						{	
							for (; i < aligmentSize; i++)
							{
								word += parseWordArr[i];
							}
							
							
							if (aligmentSize == parseWordArr.length || (aligmentSize == 0)) { }
							else
							{
								word += "-";
							}
						}
						
						writer.println(word);
						
						if (singleSpaceSelect == true)
						{
							Character = "";
							
							for(;i < parseWordArr.length;i++)
							{
								Character += parseWordArr[i];
							}
							
							if (aligmentSize != parseWordArr.length)
							{
								Character += " ";
							}
							
							lines++;
						}
						else
						{
							lines = lines + 2;
							writer.println("\n");
							
							Character = "";
							
							for(;i < parseWordArr.length;i++)
							{
								Character += parseWordArr[i];
							}
							
							if (aligmentSize != parseWordArr.length)
							{
								Character += " ";
							}
						}
					}
					else
					{
						writer.println(Space + word);
						
						if (singleSpaceSelect == true)
						{
							lines++;
						}
						else
						{
							lines = lines + 2;
							writer.println("\n");
						}
						
						Character = lineHolder.get(counter) + " ";
					}
					
					spaceCount++;
				}
				
				counter++;
			}
			
			word = Character;
			
			int aligmentSize = (maxCharacters - word.length());
			String Space = "";
			
			for (int i = 0; i < aligmentSize; i ++)
			{
				Space += " ";
			}
			
			lineLength += word.length();
			
			if (leftSideSelect == true)
			{
				writer.println(word);
				
				if (singleSpaceSelect == true)
				{
					lines++;
				}
				else
				{
					lines = lines + 2;
					writer.println("\n");
				}
			}
			else
			{
				writer.println(Space + word);
				
				if (singleSpaceSelect == true)
				{
					lines++;
				}
				else
				{
					lines = lines + 2;
					writer.println("\n");
				}
			}
			
			word = "";
			writer.close();
		}
		catch (Exception ex) 
		{
			errorLabel.setText("ERROR: The file could not be written");
		}
		
		
		//avg words per line & avg line length
		if (singleSpaceSelect == true)
		{
			aveWordLine = wordsProcessed/lines;
			aveLineLength = lineLength/lines;
		}
		else
		{
			aveWordLine = wordsProcessed/(lines/2);
			aveLineLength = lineLength/(lines/2);
			spaceCount = spaceCount/2;
			lines--;
		}
		

	}
}
